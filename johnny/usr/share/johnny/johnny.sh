#!/bin/bash

# Provides: johnny.sh
# Requires: johnny
#
export JOHNNY="/usr/share/johnny/johnny"
if [ $(id -u) -ne 0 ]; then
  $(which pkexec) env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY $JOHNNY
else
  $JOHNNY
fi
