SHELL=/bin/bash
DNAME = johnny
VERSION = "2.2.0"
ARCH = "amd64"
DESTDIR = /usr/share/
RM = /bin/rm
CP = /bin/cp
TAR = /bin/tar
MKDIR = /bin/mkdir

install:
	install -d $(DESTDIR)$(DNAME)
	install -d $(DESTDIR)doc/$(DNAME)
	${CP} ${DNAME}$(DESTDIR)$(DNAME)/*  $(DESTDIR)$(DNAME)/
	${CP} ${DNAME}$(DESTDIR)applications/* $(DESTDIR)applications/
	${CP} ${DNAME}$(DESTDIR)doc/$(DNAME)/*  $(DESTDIR)doc/$(DNAME)/
	${DNAME}/DEBIAN/postinst

uninstall:
	${RM} -rf $(DESTDIR)$(DNAME)
	${RM} -f $(DESTDIR)applications/${DNAME}*
	${RM} -rf $(DESTDIR)doc/$(DNAME)
	${DNAME}/DEBIAN/postrm

install-dependencies:
	./install_dependencies.sh

build-deb:
	make clean
	./build-deb.sh ${DNAME} ${VERSION} ${ARCH}

clean:
	${RM} -f *.deb
