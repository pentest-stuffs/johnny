# Johnny

Johnny is the cross-platform Open Source GUI frontend for the popular password
cracker John the Ripper. It was originally proposed and designed by Shinnok in
draft, version 1.0 implementation was achieved by Aleksey Cherepanov as part of
GSoC 2012 and Mathieu Laprise took Johnny further towards 2.0 and beyond as
part of GSoC 2015.

Johnny's aim is to automate and simplify the password cracking routine with the
help of the tremendously versatile and robust John the Ripper, as well as add
extra functionality on top of it, specific to Desktop and GUI paradigms, like
improved hash and password workflow, multiple attacks and session management,
easily define complex attack rules, visual feedback and statistics, all of it 
on top of the immense capabilities and features offered by both JtR core/proper
as well as jumbo

## Features
* Cross platform, builds and runs on all major desktop platforms
* Based on the most powerful and robust password cracking software, supports
both John core/proper and jumbo flavors
* Exposes most useful JtR attack modes and options in a usable, yet powerful
interface
* Simplifies password/hash management and attack results via complex filtering
and selection
* Easily define new attacks and practical multiple attack session management
* Manually guess passwords via the Guess function
* Export Passwords table to CSV and colon password file format
* Import many types of encrypted or password protected files via the 2john
functionality

Homepage: https://openwall.info/wiki/john/johnny

Johnny was tested in this linux distributions:
* Debian Stretch
* Mint

### Usage:
```
johnny
```
## INSTALLATION

### Dependencies:
Johnny need for a correct running a few another packages.

If you will install this by the `dpkg -i`, then make sure, that you have
installed all the depending packages:

```
apt-get install libc6 libgcc1 libqtcore4 libqtgui4 libstdc++6 john
```

### Install from source:
Unpack the source package and run command (like root):

```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build Debian package
Run command (like root):
```
make build-deb
```

### Install from *.deb package:
```
dpkg -i johnny*.deb
```
